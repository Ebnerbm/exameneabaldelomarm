/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.is2t1.app.views.ContactFrame;
import com.is2t1.app.views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Sistemas-10
 */
public class MainController implements ActionListener {
    
    MainFrame mainframe;
    public MainController (MainFrame mainframe){
    
        this.mainframe = mainframe;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Contactos":
                showContactFrame();
                break;
        }
    }
    public void showContactFrame(){
        ContactFrame p = new ContactFrame();
         mainframe.showChild(p, false);
    }
    
}
