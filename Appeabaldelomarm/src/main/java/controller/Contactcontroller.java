/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.is2t1.app.models.Contact;
import com.is2t1.app.views.ContactFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Sistemas-10
 */
public class Contactcontroller implements ActionListener{
    ContactFrame contactFrame;
    
    JFileChooser d;
    Contact contact;
    
    public Contactcontroller(ContactFrame f){
        super();
        contactFrame = f;
        d = new JFileChooser();
        contact = new Contact();
    }
   
    public void setContact(Contact b){
        contact = b;
    }
    public void setContact(String filePath){
        File f = new File(filePath);
        readContact(f);
    }
    public Contact getContact(){
        return contact;
    }     
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        switch (e.getActionCommand()){
            case "save":
                d.showSaveDialog(contactFrame); 
                contact = contactFrame.getContactData();
                writeContact(d.getSelectedFile());
                break;
            case "select":                
                d.showOpenDialog(contactFrame);  
                contact= readContact(d.getSelectedFile());
                contactFrame.setContactData(contact);
                break;
            case "clear":
                contactFrame.clear();
                break;

        
        }
    }
    private void writeContact(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getContact());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(Contactcontroller.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    private Contact readContact(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Contact)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(contactFrame, e.getMessage(),contactFrame.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Contactcontroller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private Contact[] readContactList(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Contact[] contacts;
        try(FileInputStream in = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(in)) {
            contacts = (Contact[]) s.readObject();
        }
        return contacts;
    }
}
